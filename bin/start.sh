#!/bin/bash

set -ex

function ep() {
   local files=($1)
   if [ -z "$2" ]; then
       local cps=$(echo ${files[@]}|tr " " "\n" | sort | tr "\n" ":" | sed 's/:$//g' | sed 's/::/:/g' | sed 's/^://g')
   else
       local cps=$(echo ${files[@]}|tr " " "\n" | sort | grep -v "$2" | tr "\n" ":" | sed 's/:$//g' | sed 's/::/:/g' | sed 's/^://g')
   fi
   echo ${cps[@]}
}

# 获取模块工作目录的绝对路径
CWD="/app/redissondemo"
ENTRANCE=${ENTRANCE:-com.klarity.redissondemo.RedissondemoApplication}
JAVA_OPTS="${JAVA_OPTS} -Dspring.profiles.active=prod"
cd ${CWD}
CMD="java -Xms512m ${JAVA_OPTS} -cp config/:$(ep "./lib/*") -jar ./lib/redissondemo-0.0.1-SNAPSHOT.jar"

exec ${CMD} $@
