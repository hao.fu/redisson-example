#!/bin/bash

DIR=$(cd `dirname $0`; pwd)

cd $DIR/..

make all

rm -rf redissondemo-release/build && mkdir redissondemo-release/build

mv redissondemo-release/redissondemo.tar.gz redissondemo-release/build/

cp build-tools/* redissondemo-release/build/

cd redissondemo-release/build

docker buildx build --platform linux/amd64 -f Dockerfile -t "public.ecr.aws/o7h3s9c6/fuhao:redissondemo" . --push
