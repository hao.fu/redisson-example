#!/bin/bash

set -ex

cd redissondemo-release/build
docker build -t ${IMAGE_NAME} .
docker push ${IMAGE_NAME}
