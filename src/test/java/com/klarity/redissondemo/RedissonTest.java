package com.klarity.redissondemo;

import org.junit.jupiter.api.Test;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RedissonTest {

    @Autowired
    private RedissonClient redissonClient;

    @Test
    void test() {

        RBucket<Object> bucket = redissonClient.getBucket("simpleObject");
        bucket.set("This is object value");
        RMap<Object, Object> map = redissonClient.getMap("simpleMap");
        map.put("mapKey", "This is map value");

        String objectValue = (String) bucket.get();
        System.out.println("stored object value: " + objectValue);
        String mapValue = (String) map.get("mapKey");
        System.out.println("stored map value: " + mapValue);
    }
}
