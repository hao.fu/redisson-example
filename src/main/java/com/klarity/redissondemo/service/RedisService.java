package com.klarity.redissondemo.service;

import com.klarity.redissondemo.model.dto.RedisTestDTO;

public interface RedisService {
    RedisTestDTO testRedis();
}
