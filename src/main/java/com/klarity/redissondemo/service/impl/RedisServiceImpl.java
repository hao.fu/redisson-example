package com.klarity.redissondemo.service.impl;

import com.klarity.redissondemo.model.dto.RedisTestDTO;
import com.klarity.redissondemo.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RedisServiceImpl implements RedisService {
    private final RedissonClient redissonClient;

    @Override
    public RedisTestDTO testRedis() {
        RBucket<Object> bucket = redissonClient.getBucket("simpleObject");
        bucket.set("This is object value");
        RMap<Object, Object> map = redissonClient.getMap("simpleMap");
        map.put("mapKey", "This is map value");

        String objectValue = (String) bucket.get();
        log.info("stored object value: " + objectValue);
        String mapValue = (String) map.get("mapKey");
        log.info("stored map value: " + mapValue);
        return RedisTestDTO.builder()
                .storedObjectValue(objectValue)
                .storedMapValue(mapValue)
                .build();
    }
}
