package com.klarity.redissondemo.controller;

import com.klarity.redissondemo.model.dto.RedisTestDTO;
import com.klarity.redissondemo.service.RedisService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/redis")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RedisController {
    private final RedisService redisServiceImpl;


    @GetMapping("/test")
    RedisTestDTO testRedis(){
        return redisServiceImpl.testRedis();
    }

}
