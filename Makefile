.PHONY: build image test clean

all: clean build


build:
	mvn -DskipTests clean package

test:
	mvn clean test

clean:
	mvn clean
